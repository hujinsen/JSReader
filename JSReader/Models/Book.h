//
//  Book.h
//  JSReader
//
//  Created by xzoscar on 16/6/22.
//  Copyright © 2016年 hujs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Book : NSObject<NSCoding, NSCopying>
@property (nonatomic, copy) NSString *rootPath;
@property (nonatomic, strong) NSMutableDictionary *manifest;

@property (nonatomic, strong) NSMutableArray *spine;

/*
 <guide>
 <reference href="index_split_042.html#filepos3202593" title="Table of Contents" type="toc"/>
 <reference href="titlepage.xhtml" title="Cover" type="cover"/>
 </guide>
 */
@property (nonatomic, strong) NSMutableDictionary *guideDic;

@property (nonatomic, strong) NSArray *catalogs;    //目录

@property (nonatomic, strong) NSMutableDictionary *chapterAndHTMLDict; //存储章节和文件的对应关系,内部使用

@property (nonatomic, copy) NSString *name;   //本书书名
@property (nonatomic, copy) NSString *selectedChapter; //当前选中的章节
@property (nonatomic, strong) NSNumber *offset;
@end
