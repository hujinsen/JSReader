//
//  Book.m
//  JSReader
//
//  Created by xzoscar on 16/6/22.
//  Copyright © 2016年 hujs. All rights reserved.
//

#import "Book.h"

@implementation Book

- (instancetype)init
{
    self = [super init];
    if (self) {
        _offset = [NSNumber numberWithFloat:1.0];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.rootPath forKey:@"rootPath"];
    [aCoder encodeObject:self.manifest forKey:@"manifest"];
    [aCoder encodeObject:self.spine forKey:@"spine"];
    [aCoder encodeObject:self.guideDic forKey:@"guideDic"];
    [aCoder encodeObject:self.catalogs forKey:@"catalogs"];
    [aCoder encodeObject:self.chapterAndHTMLDict forKey:@"chapterAndHTMLDict"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.selectedChapter forKey:@"selectedChapter"];
    [aCoder encodeObject:self.offset forKey:@"offset"];
}
- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        _rootPath = [aDecoder decodeObjectForKey:@"rootPath"];
        _manifest = [aDecoder decodeObjectForKey:@"manifest"];
        _spine = [aDecoder decodeObjectForKey:@"spine"];
        _guideDic = [aDecoder decodeObjectForKey:@"guideDic"];
        _catalogs = [aDecoder decodeObjectForKey:@"catalogs"];
        _chapterAndHTMLDict = [aDecoder decodeObjectForKey:@"chapterAndHTMLDict"];
        _name = [aDecoder decodeObjectForKey:@"name"];
        _selectedChapter = [aDecoder decodeObjectForKey:@"selectedChapter"];
        _offset = [aDecoder decodeObjectForKey:@"offset"];
    }
    return self;
}

- (id)copyWithZone:(nullable NSZone *)zone {
    Book *b = [[Book alloc] init];
    b.rootPath = [self.rootPath copy];
    b.manifest = [self.manifest mutableCopy];
    b.spine = [self.spine mutableCopy];
    b.guideDic = [self.guideDic mutableCopy];
    b.catalogs = [self.catalogs copy];
    b.chapterAndHTMLDict = [self.chapterAndHTMLDict mutableCopy];
    b.name = [self.name copy];
    b.selectedChapter = [self.selectedChapter copy];
    b.offset = [NSNumber numberWithFloat:[self.offset floatValue]];
    return  b;
}
@end
