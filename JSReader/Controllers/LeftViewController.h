//
//  LeftViewController.h
//  JSReader
//
//  Created by xzoscar on 16/6/20.
//  Copyright © 2016年 hujs. All rights reserved.
//

#import "BaseViewController.h"

@interface LeftViewController : BaseViewController
@property (nonatomic, weak) IBOutlet UITableView *tableView;
- (IBAction)bookShelfBtnAct:(UIButton *)sender;
- (IBAction)settingBtnAct:(UIButton *)sender;
@end
