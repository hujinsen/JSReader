//
//  LeftViewCell.m
//  JSReader
//
//  Created by xzoscar on 16/6/23.
//  Copyright © 2016年 hujs. All rights reserved.
//

#import "LeftViewCell.h"
#import "Book.h"
@implementation LeftViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setBook:(Book *)book index:(NSInteger)index {
    self.textLabel.text = book.catalogs[index];
    if ([self.textLabel.text isEqualToString:book.selectedChapter]) {
        [self setSelected:YES animated:YES];
        self.textLabel.textColor = [UIColor greenColor];
    }else {
        [self setSelected:NO animated:YES];
        self.textLabel.textColor = [UIColor whiteColor];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
