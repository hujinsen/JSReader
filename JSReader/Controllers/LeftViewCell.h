//
//  LeftViewCell.h
//  JSReader
//
//  Created by xzoscar on 16/6/23.
//  Copyright © 2016年 hujs. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Book;
@interface LeftViewCell : UITableViewCell
- (void)setBook:(Book *)book index:(NSInteger) index;
@end
