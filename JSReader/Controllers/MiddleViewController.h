//
//  MiddleViewController.h
//  JSReader
//
//  Created by xzoscar on 16/6/27.
//  Copyright © 2016年 hujs. All rights reserved.
//

#import "TOWebViewController.h"

@interface MiddleViewController : TOWebViewController <UIScrollViewDelegate>

@end
