//
//  LeftViewController.m
//  JSReader
//
//  Created by xzoscar on 16/6/20.
//  Copyright © 2016年 hujs. All rights reserved.
//

#import "LeftViewController.h"
#import "LeftViewCell.h"
#import "JSFileHandler.h"
#import "UIViewController+MMDrawerController.h"
#import "AppDelegate.h"

@interface LeftViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    Book  *_book;
}
@end

@implementation LeftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.backgroundColor = [UIColor clearColor];
    [_tableView registerNib:[UINib nibWithNibName:@"LeftViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   
     _book = [[JSFileHandler shared] selectedBook];
    [_tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _book.catalogs.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LeftViewCell *cell = (LeftViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell setBook:_book index:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _book.selectedChapter = _book.catalogs[indexPath.row];
//    [[NSNotificationCenter defaultCenter]
//     postNotificationName:kLoadChapterNotify
//     object:nil
//     userInfo:@{
//                kChapterName : _book.selectedChapter,
//                kBook: _book
//                }];
    [[NSNotificationCenter defaultCenter] postNotificationName:kRefreshContentsNotify object:nil];
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)bookShelfBtnAct:(UIButton *)sender {
    //去除最后一次阅读书籍
    [[JSFileHandler shared] removeReadRecord];
    
    AppDelegate *del = [UIApplication sharedApplication].delegate;
    
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
     window.rootViewController = del.shelfVC;
   
    
    [UIView transitionWithView:window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:nil];
}

#pragma mark - actions

- (IBAction)settingBtnAct:(UIButton *)sender {
    [self.mm_drawerController openDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}
@end
