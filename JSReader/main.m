//
//  main.m
//  JSReader
//
//  Created by xzoscar on 16/6/19.
//  Copyright © 2016年 hujs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
