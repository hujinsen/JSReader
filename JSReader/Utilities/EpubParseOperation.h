//
//  EpubParseOperation.h
//  JSReader
//
//  Created by xzoscar on 16/6/23.
//  Copyright © 2016年 hujs. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Book;
typedef void (^completion) (Book *book);
@interface EpubParseOperation : NSOperation
- (instancetype)initWithBookName:(NSString *)bookName completion:(completion)block NS_DESIGNATED_INITIALIZER;
@end
