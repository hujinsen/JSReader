//
//  JSFileHandler.h
//  JSReader
//
//  Created by xzoscar on 16/6/19.
//  Copyright © 2016年 hujs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Book.h"

typedef void(^completionHandler)(Book *selectedBook);

@interface JSFileHandler : NSObject

+ (id)shared;

- (BOOL)updateBookList;

- (NSDictionary *)allBookNameAndPath;

- (NSArray *)allBooks;

- (Book *)selectedBook;

- (void)setSelectedBookName:(NSString *)name;

- (NSString *)selectedBookName;

- (void)loadBookWithName:(NSString *)bookName lastReadBook:( Book* _Nullable )lastReadBook completionHandler:(completionHandler)block;

- (void)saveReadRecord:(NSNumber *)offset;
- ( Book * _Nullable )readLastReadBook:(NSString *)bookName;

- (void)removeReadRecord;
@end

