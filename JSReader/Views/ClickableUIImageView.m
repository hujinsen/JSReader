//
//  ClickableUIImageView.m
//  JSReader
//
//  Created by xzoscar on 16/6/19.
//  Copyright © 2016年 hujs. All rights reserved.
//

#import "ClickableUIImageView.h"

@implementation ClickableUIImageView {
    UIView *_mask;
    actionBlock _block;
    NSString *_bookName;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
         self.userInteractionEnabled = YES;
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame imageName:(NSString *)name handleClick:(actionBlock)block {
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        self.image = [UIImage imageNamed:name];
        _bookName = [name stringByDeletingPathExtension];
        if (block) {
            _block = [block copy];
        }
    }
    return self;
}
- (void)handleClick:(actionBlock) block {
    if (block) {
        _block = [block copy];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!_mask) {
        _mask = [[UIView alloc] initWithFrame:self.bounds];
        _mask.backgroundColor = [UIColor blackColor];
        _mask.alpha = 0.5;
        [self addSubview:_mask];
           }else{
        _mask.hidden = NO;
    }
}

/**
 *  重载touch方法
 *
 *  @param touches touches description
 *  @param event   event description
 */
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (_block) {
        _block(_bookName);
    }
    _mask.hidden=YES;
    
}


-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    _mask.hidden=YES;

}


@end
