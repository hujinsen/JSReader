//
//  ClickableUIImageView.h
//  JSReader
//
//  Created by xzoscar on 16/6/19.
//  Copyright © 2016年 hujs. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^actionBlock)(NSString *bookName);

@interface ClickableUIImageView : UIImageView

- (instancetype)initWithFrame:(CGRect)frame imageName:(NSString *)name handleClick:(actionBlock)block NS_DESIGNATED_INITIALIZER;


@end
